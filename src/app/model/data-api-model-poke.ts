export class DataApiModelPoke {
  id: number;
  name: string;
  type: string;
  sexPics: string;
  pokePics: string;
  littlePokePics: string;
  pokeResum: string;
}
