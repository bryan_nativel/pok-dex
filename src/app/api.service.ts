import { DataApiModelPoke } from "./model/data-api-model-poke";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";
  apiURL: string = "http://localhost:3000";
  constructor(private httpClient: HttpClient) {}

  public getPokemonById(id: number) {
    return this.httpClient.get(`${this.apiURL}/pokemon/${id}`);
  }

  public getPokemons(url?: string) {
    return this.httpClient.get<DataApiModelPoke[]>(`${this.apiURL}/pokemon`);
  }
}
