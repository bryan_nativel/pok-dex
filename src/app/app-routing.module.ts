import { HomeComponent } from "./home/home.component";
import { DetailPokemonComponent } from "./detail-pokemon/detail-pokemon.component";
import { ListePokemonComponent } from "./liste-pokemon/liste-pokemon.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const ROUTING: Routes = [
  { path: "", component: HomeComponent },
  { path: "pokedex", component: ListePokemonComponent },
  { path: "detailPokedex/:id", component: DetailPokemonComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      ROUTING,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
})
export class AppRoutingModule {}
