import { ApiService } from "./../api.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-detail-pokemon",
  templateUrl: "./detail-pokemon.component.html",
  styleUrls: ["./detail-pokemon.component.css"],
})
export class DetailPokemonComponent implements OnInit {
  idPokemon: number;
  name: string;
  type: string;
  sexPics: string;
  pokePics: string;
  littlePokePics: string;
  pokeResum: string;

  constructor(private route: ActivatedRoute, private apiService: ApiService) {}
  ngOnInit() {
    let tmp: string;
    this.route.paramMap.subscribe((params) => {
      tmp = params.get("id");
      this.idPokemon = parseInt(tmp);
    });
    this.apiService.getPokemons().subscribe((res) => {
      this.apiService.getPokemons(this.apiService.nextPage).subscribe((res) => {
        this.getPokemon(res);
      });
    });
  }

  public getPokemon(param) {
    let tmp;
    for (const key in param) {
      if (param[key].id === this.idPokemon) {
        console.log(param[key]);
        this.name = param[key].name;
        this.type = param[key].type;
        this.sexPics =
          param[key].sexPics === "/assets/picture/mal.png"
            ? (this.sexPics = "/assets/picture/male.png")
            : (this.sexPics = "/assets/picture/male.png");
        this.pokePics = param[key].pokePics;
        this.littlePokePics = param[key].littlePokePics;
        this.pokeResum = param[key].pokeResum;
      }
    }
  }
}
