import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";

@Component({
  selector: "app-liste-pokemon",
  templateUrl: "./liste-pokemon.component.html",
  styleUrls: ["./liste-pokemon.component.css"],
})
export class ListePokemonComponent implements OnInit {
  constructor(public apiService: ApiService) {}
  pokemon: {};
  ngOnInit() {
    this.apiService.getPokemons().subscribe((res) => {
      this.apiService.getPokemons(this.apiService.nextPage).subscribe((res) => {
        this.pokemon = res;
        console.log(this.pokemon);
      });
    });
  }
}
